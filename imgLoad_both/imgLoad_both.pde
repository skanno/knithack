import controlP5.*;
import javax.swing.*;
import sojamo.drop.*;

ControlP5 cp5;
PImage img;
SDrop drop;

String getFile = null;

void setup(){
  size(600,600);
  cp5 = new ControlP5(this);
  cp5.addButton("loadImageFile")
       //.setValue(0)
       .setPosition(450,10)
       .setSize(100,30);
  drop = new SDrop(this);
}

void draw(){
  //background(0);
  //image(img, 10, 10, 400, 400); 
  if(getFile != null){
    fileLoader();
  }
  
  if(img != null){
    image(img, 10, 10, 400, 400);
  }
}

public void loadImageFile(int theValue){
  getFile = getFileName();
}

//ファイルを取り込むファンクション 
void fileLoader(){
  //選択ファイルパスのドット以降の文字列を取得
  String ext = getFile.substring(getFile.lastIndexOf('.') + 1);
  //その文字列を小文字にする
  ext.toLowerCase();
  //文字列末尾がjpg,png,gif,tgaのいずれかであれば 
  if(ext.equals("jpg") || ext.equals("png") ||  ext.equals("gif") || ext.equals("tga")){
    //選択ファイルパスの画像を取り込む
    img = loadImage(getFile);
    //イメージ表示 
    //image(pimage, 0, 0, pimage.width, pimage.height); 
    image(img, 10, 10, 400, 400); 
  }
  //選択ファイルパスを空に戻す
  getFile = null; 
}

//ファイル選択画面、選択ファイルパス取得の処理 
String getFileName(){
  //処理タイミングの設定 
  SwingUtilities.invokeLater(new Runnable() { 
    public void run() {
 try {
   //ファイル選択画面表示 
   JFileChooser fc = new JFileChooser(); 
   int returnVal = fc.showOpenDialog(null);
   //「開く」ボタンが押された場合
   if (returnVal == JFileChooser.APPROVE_OPTION) {
     //選択ファイル取得 
     File file = fc.getSelectedFile();
     //選択ファイルのパス取得 
     getFile = file.getPath(); 
   } 
 }
 //上記以外の場合 
 catch (Exception e) {
   //エラー出力 
   e.printStackTrace(); 
 } 
    } 
  } 
  );
  //選択ファイルパス取得
  return getFile; 
}

void dropEvent(DropEvent theDropEvent) {
  println("");
  println("isFile()\t"+theDropEvent.isFile());
  println("isImage()\t"+theDropEvent.isImage());
  println("isURL()\t"+theDropEvent.isURL());
  
  // if the dropped object is an image, then 
  // load the image into our PImage.
  if(theDropEvent.isImage()) {
    println("### loading image ...");
    img = theDropEvent.loadImage();
    //image(img, 10, 10, 400, 400); 
  }
}

